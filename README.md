# Random Line

This library returns a random line for a given file, but you must provide your
own file.

# Preparing your environment

* Install [Node and npm](https://https://nodejs.org/en/)
* Create an npm account here:
  [Node Package Manager](https://www.npmjs.com/signup)
* Go to [GitLab](http://gitlab.com) and create an account.

# Pushing your code there

* Create a new repository, and push this code there.
* run `npm login`
* run `npm init`
* Make sure to include your username on the package name. Example:
  `@melomario/random-line`

_A wild new file appears_, and it's called `package.json`.
